package br.com.vendas.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.sun.xml.bind.v2.runtime.output.Encoded;

import br.com.vendas.domain.entity.Usuario;
import br.com.vendas.domain.repository.UsuarioRepository;
import br.com.vendas.exception.SenhaInvalidaException;

@Service
public class UsuarioServiceImpl implements UserDetailsService{

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Transactional
	public Usuario salvar(Usuario usuario){
		return usuarioRepository.save(usuario);
	}
	 
	public UserDetails autenticar(Usuario usuario) {
		
		UserDetails userDetails = loadUserByUsername(usuario.getLogin());
		
		boolean senhasBatem = passwordEncoder.matches(usuario.getSenha(), userDetails.getPassword());
		
		if(senhasBatem) {
			return userDetails;
		}
		
		throw new SenhaInvalidaException();
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Usuario usuario = usuarioRepository
			.findByLogin(username)
			.orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado na base de dados !"));
		
		String[] roles = usuario.isAdmin() ? new String[] {"ADMIN", "USER"} : new String[] {"USER"}; 
		
		return User
				.builder()
				.username(usuario.getLogin())
				.password(usuario.getSenha())
				.roles(roles)
				.build();
	}
}