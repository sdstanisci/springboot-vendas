package br.com.vendas.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import br.com.vendas.domain.entity.Cliente;
import br.com.vendas.domain.entity.ItemPedido;
import br.com.vendas.domain.entity.Pedido;
import br.com.vendas.domain.entity.Produto;
import br.com.vendas.domain.enums.StatusPedido;
import br.com.vendas.domain.repository.ClienteRepository;
import br.com.vendas.domain.repository.ItemPedidoRepository;
import br.com.vendas.domain.repository.PedidoRepository;
import br.com.vendas.domain.repository.ProdutoRepository;
import br.com.vendas.exception.PedidoNaoEncontradoException;
import br.com.vendas.exception.RegraNegocioException;
import br.com.vendas.rest.dto.ItemPedidoDTO;
import br.com.vendas.rest.dto.PedidoDTO;
import br.com.vendas.service.PedidoService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PedidoServiceImpl implements PedidoService{

	private final PedidoRepository pedidoRepository;
	private final ClienteRepository clienteRepository;
	private final ProdutoRepository produtoRepository;
	private final ItemPedidoRepository itemPedidoRepository;
	
	@Override
	@Transactional
	public Pedido salvar(PedidoDTO dto) {
		
		Integer idCliente = dto.getCliente();
		
		Cliente cliente = clienteRepository.findById(idCliente)
			.orElseThrow( () -> new RegraNegocioException("Código Cliente inválido !"));
		
		Pedido pedido = new Pedido();
		pedido.setTotal(dto.getTotal());
		pedido.setDataPedido(LocalDate.now());
		pedido.setCliente(cliente);
		pedido.setStatus(StatusPedido.REALIZADO);
		
		List<ItemPedido> itensPedido = converterItens(pedido, dto.getItens());
		
		pedidoRepository.save(pedido);
		
		itemPedidoRepository.saveAll(itensPedido);
		
		pedido.setItens(itensPedido);
		
		return pedido;
	}
	
	private List<ItemPedido> converterItens(Pedido pedido, List<ItemPedidoDTO> itens) {
		
		if(itens.isEmpty()) {
			throw new RegraNegocioException("Lista de intens vazia.");
		}
		
		return itens
				.stream()
				.map( dto -> {
					
					Integer idProduto = dto.getProduto();
					Produto produto = produtoRepository
										.findById(idProduto)
										.orElseThrow(() -> new RegraNegocioException("Código do Produto " + idProduto + " Inválido !"));
					
					ItemPedido itemPedido = new ItemPedido();
					
					itemPedido.setQuantidade(dto.getQuantidade());
					itemPedido.setPedido(pedido);		
					itemPedido.setProduto(produto);
					
					return itemPedido;
					
				}).collect(Collectors.toList());
		
	}

	@Override
	public Optional<Pedido> obterPedidoCompleto(Integer id) {
		return pedidoRepository.findByIdFetchItens(id);
	}

	@Override
	@Transactional
	public void atualizaStatus(Integer id, StatusPedido statusPedido) {
		
		pedidoRepository
			.findById(id)
			.map(pedido -> {
				pedido.setStatus(statusPedido);
				return pedidoRepository.save(pedido);
			}).orElseThrow(() -> new PedidoNaoEncontradoException());
	}
}
