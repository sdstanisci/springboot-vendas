package br.com.vendas.rest.dto;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotNull;

import br.com.vendas.validation.NotEmptyList;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PedidoDTO {

	@NotNull(message = "{campo.codigo-cliente.obrigatorio}")
	private Integer cliente;
	
	@NotNull(message = "{campo.total-pedido.obrigatorio}")
	private BigDecimal total;
	
	@NotEmptyList(message = "{campo.itens-pedido.obrigatorio}")
	private List<ItemPedidoDTO> itens;
	
}
