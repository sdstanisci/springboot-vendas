	package br.com.vendas.rest.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import br.com.vendas.domain.entity.Produto;
import br.com.vendas.domain.repository.ProdutoRepository;


@RestController
@RequestMapping("/api/produto")
public class ProdutoController {

	private ProdutoRepository repository;

	public ProdutoController(ProdutoRepository repository) {
		this.repository = repository;
	}

	@GetMapping("{id}")
	public Produto getProdutoById(@PathVariable Integer id) {

		return repository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Produto não encontrado."));
	}

	@PostMapping
	@ResponseStatus(CREATED)
	public Produto save(@RequestBody @Valid Produto produto) {

		return repository.save(produto);
	}

	@DeleteMapping("{id}")
	@ResponseStatus(NO_CONTENT)
	public void delete(@PathVariable Integer id) {

		repository.findById(id).map(produto -> {
			repository.delete(produto);
			return produto;
		}).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Cliente não encontrado."));
	}

	@PutMapping("{id}")
	@ResponseStatus(NO_CONTENT)
	public void update(@PathVariable Integer id, @RequestBody @Valid Produto produto) {

		repository
			.findById(id)
			.map(produtoEncontrado -> {
				produto.setId(produtoEncontrado.getId());
				repository.save(produto);
				return produto;
			}).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Produto não encontrado."));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("filtro")
	public List<Produto> find(Produto filtro) {

		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		
		Example example = Example.of(filtro, matcher);

		return repository.findAll(example);

	}
}
