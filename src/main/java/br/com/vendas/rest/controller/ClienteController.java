package br.com.vendas.rest.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import br.com.vendas.domain.entity.Cliente;
import br.com.vendas.domain.repository.ClienteRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/cliente")
@Api("Api cliente")
public class ClienteController {

	private ClienteRepository repository;

	public ClienteController(ClienteRepository repository) {
		this.repository = repository;
	}

	@GetMapping("{id}")
	@ApiOperation("Obter detalhes de um cliente")
	@ApiResponses({
			@ApiResponse(code=200, message = "Cliente encontrado"),
			@ApiResponse(code = 404, message = "Cliente não econtrado para o Id informado.")
			})
	public Cliente getClienteById(@PathVariable @ApiParam("Id do cliente") Integer id) {

		return repository
				.findById(id)
				.orElseThrow( () -> 
					new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado.")
				);
	}

	@PostMapping
	@ResponseStatus( HttpStatus.CREATED )
	@ApiOperation("Salva um novo cliente")
	@ApiResponses({
			@ApiResponse(code=201, message = "Cliente salvo com sucesso."),
			@ApiResponse(code = 400, message = "Erro de validação")
			})
	public Cliente save(@RequestBody @Valid Cliente cliente) {

		return repository.save(cliente);
	}

	@DeleteMapping("{id}")
	@ResponseStatus( HttpStatus.NO_CONTENT )
	public void delete(@PathVariable Integer id) {

		repository.findById(id)
			.map( cliente -> {
				repository.delete(cliente);
				return cliente;
			})
			.orElseThrow( () -> 
			new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado.")
		);

	}

	@PutMapping("{id}")
	@ResponseStatus( HttpStatus.NO_CONTENT )
	public void update(@PathVariable Integer id, @RequestBody Cliente cliente) {
		
		repository
			.findById(id)
			.map(clienteEncontrado -> {
				cliente.setId(clienteEncontrado.getId());
				repository.save(cliente);
				return clienteEncontrado;
			})
			.orElseThrow( () -> 
				new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado.")
			);
	}
	

	@SuppressWarnings({ "rawtypes", "unchecked" })	
	@GetMapping("filtro")
	public List<Cliente> find(Cliente filtro) {

		ExampleMatcher matcher = ExampleMatcher
									.matching()
									.withIgnoreCase()
									.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
		
		Example example = Example.of(filtro, matcher);
		
		return repository.findAll(example);
				
	}
}
