package br.com.vendas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class VendasApplication extends SpringBootServletInitializer{

//	@Bean(name = "testeCliente")
//	public CommandLineRunner init(@Autowired ClienteRepository clienteRepository,
//								 @Autowired PedidoRepository pedidoRepository ) {
//		
//		return args -> {
//
//			System.out.print("Salvando clientes");
//			
//			Cliente fulano = clienteRepository.save(new Cliente("Sergio"));
//			clienteRepository.save(fulano);
//			
//			Pedido pedido = new Pedido();
//			pedido.setCliente(fulano);
//			pedido.setDataPedido(LocalDate.now());
//			pedido.setTotal(BigDecimal.valueOf(100.00));
//			
//			pedidoRepository.save(pedido);
//			
//			//pedidoRepository.findByCliete(fulano).forEach(System.out::println);
//			
//		};
//	}

	public static void main(String[] args) {
		SpringApplication.run(VendasApplication.class, args);
	}

}
