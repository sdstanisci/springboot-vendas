package br.com.vendas.domain.jdbctemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vendas.domain.entity.Cliente;

@Repository
public class ClienteJDBCTemplateRepository{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private EntityManager entityManager;
	

	private String INSERT = "insert into cliente (nome) values (?)";
	private String UPDATE = "update cliente set nome = ? where id = ?";
	private String SELECT_ALL = "select * from cliente";
	private String DELETE = "delete from cliente where id = ? ";
	private String SELECT_NAME = "select * from cliente where UPPER(nome) like ? ";

	@Transactional
	public Cliente salvar(Cliente cliente) {

		//jdbcTemplate.update(INSERT, new Object[] { cliente.getNome() });
		entityManager.persist(cliente);
			
		return cliente;
	}

	@Transactional
	public Cliente atualizar(Cliente cliente) {

		//jdbcTemplate.update(UPDATE, new Object[] { cliente.getNome(), cliente.getId() });
		 entityManager.merge(cliente);
		
		return cliente;
	}

	@Transactional
	public void deletar(Integer id) {

		//jdbcTemplate.update(DELETE, new Object[] { id });
		Cliente cliente = entityManager.find(Cliente.class, id);
		deletar(cliente);
	}
	
	@Transactional
	public void deletar(Cliente cliente) {
		entityManager.remove(cliente);
	}

	public List<Cliente> obterTodosClientes() {
		
		return jdbcTemplate.query(SELECT_ALL, obterClienteMapper());
	}
	
	@Transactional(readOnly = true)
	public List<Cliente> buscarPorNome(String nome){
				
		return jdbcTemplate.query(SELECT_NAME, 
				new Object[] {"%".concat(nome.toUpperCase()).concat("%")}, 
				obterClienteMapper());
	}

	private RowMapper<Cliente> obterClienteMapper() {

		return new RowMapper<Cliente>() {

			@Override
			public Cliente mapRow(ResultSet resultSet, int i) throws SQLException {

				return new Cliente(resultSet.getInt("id"), resultSet.getString("nome"));
			}
		};
	}
}
